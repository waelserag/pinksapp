{{-- Start Table --}}
<table class="table table-dark search-table">
  <thead>
    <th>#</th>
    <th>Name</th>
    <th>Employee Responsible</th>
    <th>Category</th>
  </thead>
  <tbody>
    @foreach ($services as $index => $service)
    <tr class="table-active">
      <td>{{ $index+1 }}</td>
      <td>{{ $service->name }}</td>
      <td>{{ $service->employee->name }}</td>
      <td>{{ $service->category->name }}</td>
    </tr>
    @endforeach
  </tbody>
</table>
{{-- End Table --}}

{{-- Start Pagination --}}
<div class="custom-pagination"  id="pagination_data">
  {{ $services->withQueryString()->links("pagination::bootstrap-4") }}
</div>
{{-- End Pagination --}}