@extends('layouts.app')

@section('breadcrumb')
<nav aria-label="breadcrumb" class="breadcrumb-content">
  <ol class="breadcrumb bg-light p-3 border border-warning border-2">
    <li class="breadcrumb-item"><a href="{{ url('/services') }}">Services</a></li>
    <li class="breadcrumb-item active" aria-current="page">Add Service</li>
  </ol>
</nav>
@endsection

@section('content')
<form method="POST" action="{{ url("services") }}">
    @csrf
  <div class="mb-3">
    <label for="name" class="form-label">Name</label>
    <input type="text" class="form-control" name="name" id="name" value="{{ old('name') }}">
  </div>
  <div class="mb-3">
    <label for="employee_id" class="form-label">Employee Responsible</label>
    <select name="employee_id" class="form-control" id="employee_id">
      @foreach ($employees as $employee)
        <option value="{{ $employee->id }}" {{ $employee->id == old('employee_id') ? "selected" : "" }}>{{ $employee->name }}</option>
      @endforeach
    </select>
  </div>
  <div class="mb-3">
    <label for="category_id" class="form-label">Category</label>
    <select name="category_id" class="form-control" id="category_id">
      @foreach ($categories as $category)
        <option value="{{ $category->id }}" {{ $category->id == old('category_id') ? "selected" : "" }}>{{ $category->name }}</option>
      @endforeach
    </select>
  </div>
  <button type="submit" class="btn btn-primary">Save</button>
</form>
@endsection