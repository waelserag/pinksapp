@extends('layouts.app')

@section('breadcrumb')
<nav aria-label="breadcrumb" class="breadcrumb-content">
  <ol class="breadcrumb bg-light p-3 border border-warning border-2">
    <li class="breadcrumb-item active" aria-current="page">Services</li>
  </ol>
</nav>
@endsection

@section('content')
{{-- Start Search --}}
<form  id="searchform" class="search">
  <div class="container">
    <div class="row">
      <div class="col-md-3 mb-3">
        <label for="name" class="form-label">Name</label>
        <input type="text" class="form-control" value="{{ request()->name }}" name="name" id="name">
      </div>
      <div class="col-md-3 mb-3">
        <label for="employee_id" class="form-label">Employee Responsible</label>
        <select name="employee_id" class="form-control" id="employee_id">
          <option value="">All</option>
          @foreach ($employees as $employee)
            <option value="{{ $employee->id }}" {{ $employee->id == request()->employee_id ? "selected" : "" }}>{{ $employee->name }}</option>
          @endforeach
        </select>
      </div>
      <div class="col-md-3 mb-3">
        <label for="category_id" class="form-label">Category</label>
        <select name="category_id" class="form-control" id="category_id">
          <option value="">All</option>
          @foreach ($categories as $category)
            <option value="{{ $category->id }}" {{ $category->id == request()->category_id ? "selected" : "" }}>{{ $category->name }}</option>
          @endforeach
        </select>
      </div>
      <div class="col-md-3">
        <button id='search_btn' class="btn btn-primary search-button">Search</button>
      </div>
    </div>
  </div>
</form>
{{-- End Search --}}

{{-- Start Pagination --}}
<div class="custom-pagination"  id="pagination_data">
  @include("services.service-pagination",["services"=>$services])
</div>
{{-- End Pagination --}}
@endsection

@section('scripts')
<script>
    $(function() {
      var someFunction = function (event) {
        event.preventDefault();
        //get url and make final url for ajax 
        var url = "{{ route('services.index') }}"
        var append = url.indexOf("?") == -1 ? "?" : "&";
        var finalURL = url + append + $("#searchform").serialize();

        //set to current url
        window.history.pushState({}, null, finalURL);

        $.get(finalURL, function(data) {

          $("#pagination_data").html(data);

        });

        return false;
      }
      // Add our event listeners
      let btn = document.getElementById("search_btn")
      let name = document.getElementById("name")
      let category_id = document.getElementById("category_id")
      let employee_id = document.getElementById("employee_id")
      btn.addEventListener('click', someFunction, false);
      name.addEventListener('keyup', someFunction, false);
      category_id.addEventListener('change', someFunction, false);
      employee_id.addEventListener('change', someFunction, false);
    });
  </script>
@endsection