<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pinksapp</title>
    {{-- Bootstrap5 CSS --}}
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    {{-- Custom CSS --}}
    <link rel="stylesheet" href="{{ asset("assets/css/style.css") }}">
</head>
<body>
    @include('header')
    <div class="container">
        <div class="row">
            @include('messages')
            @yield('breadcrumb')
            <div class="card col-md-12">
                @yield('content')
            </div>
        </div>
    </div>
    @include('footer')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    @yield('scripts')
  	@stack('scripts')
</body>
</html>