<header id="header" class="header-section navbar-fixed-top">
    <div class="container">
        <nav class="navbar">
            <a href="{{ url('/services') }}" class="navbar-brand">
                <img class="logo-light" src="https://system.pinksapp.com/public/assets/web2/img/logo-light.png" alt="Appium">
            </a>
            <div class="d-flex menu-wrap">
                <div id="navmenu" class="mainmenu">
                    <ul class="nav">
                        <li><a data-scroll="" class="nav-link active" href="{{ url('/services') }}">Services</a></li>
                        <li><a data-scroll="" class="nav-link active" href="{{ url('/services/create') }}">Add Service</a></li>
                    </ul>

                </div>
            </div>
        </nav>
    </div>
</header>