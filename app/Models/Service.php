<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasFactory;

    /**
     * Summary of fillable
     * @var mixed
     */
    protected $fillable = ["name", "employee_id", "category_id"];

    /**
     * Get the employee responsible for the service.
     */
    public function employee()
    {
        return $this->belongsTo(Employee::class)->withDefault('');
    }

    /**
     * Get the category for the service.
     */
    public function category()
    {
        return $this->belongsTo(Category::class)->withDefault('');
    }
}
