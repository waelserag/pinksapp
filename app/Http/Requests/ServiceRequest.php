<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ServiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules() :array
    {
        return [
            'name'        => 'required|string|min:3|max:100|unique:services,name',
            'employee_id' => 'required|exists:employees,id',
            'category_id' => 'required|exists:categories,id',
        ];
    }

    /**
     * Rename Attribute
     */
    public function attributes()
    {
        return [
            'name'        => 'Service Name',
            'employee_id' => 'Emplyee Responsible',
            'category_id' => 'Category Name',
        ];
    }
}
