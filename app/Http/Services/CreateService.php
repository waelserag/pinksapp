<?php

namespace App\Http\Services;

use App\Models\Service;
use Symfony\Component\HttpFoundation\Request;

class CreateService
{
    /**
     * Add New Service
     *
     * @param  Request  $service (name, employee_responsible, category)
     * @return \App\Models\Service
     */
    public function fillFromRequest(Request $request) :Service
    {
        $service = Service::create($request->request->all());
        return $service;
    }
}
