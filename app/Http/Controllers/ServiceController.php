<?php

namespace App\Http\Controllers;

use App\Http\Services\CreateService;
use App\Http\Requests\ServiceRequest;
use App\Http\Resources\ServiceResource;
use App\Models\Category;
use App\Models\Employee;
use App\Models\Service;
use EloquentBuilder;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class ServiceController extends Controller
{
    protected $createService;
    public function __construct(CreateService $createService){
        $this->createService = $createService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index() :view
    {
        // Get Categories And Employees To Select From their
        $categories = Category::get(['id', 'name']);
        $employees  = Employee::get(['id', 'name']);

        //Search
        $services = EloquentBuilder::to(Service::class,request()->only('name', 'category_id', 'employee_id'));

        //Get Data With Pagination
        $services = new ServiceResource($services->with('category:id,name', 'employee:id,name')->paginate(20));
        
        //Return If This is From Ajax
        if(request()->ajax()){
            return view('services.service-pagination ',['services' => $services, 'employees' => $employees, 'categories' => $categories]); 
        } 
        
        //Return If This is Not From Ajax
        return view('services.index',['services' => $services, 'employees' => $employees, 'categories' => $categories]);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\View\View
    */
   public function create() :View
   {
        $categories = Category::get(['id', 'name']);
        $employees  = Employee::get(['id', 'name']);
        return view('services.create',compact("categories", "employees"));
   }

    /**
     * Store a newly created service.
     *
     * @param  ServiceRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ServiceRequest $request) :RedirectResponse
    {
        $response = $this->createService->fillFromRequest($request);
        return redirect()->back()->with(['success' => "Created Successfully"]);
    }
}
