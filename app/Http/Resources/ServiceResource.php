<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ServiceResource extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) :array
    {
       return [
            'name'       => $this->name,
            'employee_id'=> $this->employee_id,
            'category_id'=> $this->category_id,
        ];

    }
}
